interface Sayable { // functional interface...
    public void say(String name);
    // int draw();
}

interface Drawable { // functional interface...
    public void draw();
}

interface Running { // functional interface...
    void run();
}

interface constructorReferncing { // functional interface...
    void runConstructor();
}

public class Java8_lambda {
    Java8_lambda() {
        System.out.println("constructor running...");
    }

    static void saySomething(String name) {
        System.out.println(" i have nothing to say..." + name);
    }

    static void drawSomething() {
        System.out.println(" i have nothing to draw...");
    }

    void running() {
        System.out.println("i am running fast....");
    }

    public static void main(String[] args) {
        Sayable s1 = (name) -> { // lambda expression
            System.out.println("How are you? .. " + name);
        };
        s1.say("lalan");

        Drawable d1 = () -> { // lambda expression
            System.out.println("Drawing.....");
        };
        d1.draw();

        Sayable s2 = Java8_lambda::saySomething; // Static method referencing
        s2.say("lalan");
        Drawable d2 = Java8_lambda::drawSomething;// Static method referencing
        d2.draw();

        Java8_lambda j1 = new Java8_lambda();

        Running r1 = j1::running; // intance method referencing
        r1.run();

        constructorReferncing c1 = Java8_lambda::new; // construtor referencing
        c1.runConstructor();

    }
}
