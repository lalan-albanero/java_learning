import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class HashSetCollection {
    public static void main(String[] args) {
        HashSet <String> hash2=new HashSet<>();
        hash2.add("one");
        hash2.add("two");
        hash2.add("three");
        hash2.add("four");
        System.out.println(hash2);
        LinkedHashSet <String> hash1=new LinkedHashSet<>();
        hash1.add("five");
        hash1.add("six");
        hash1.add("seven");
        System.out.println(hash1);
        hash2.addAll(hash1);
        hash2.removeIf(str->str.contains("one"));
        System.out.println(hash2);

        TreeSet <String> tree=new TreeSet<>(hash2);
        System.out.println(tree);
        Iterator <String> itr=tree.descendingIterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }


    }
}
