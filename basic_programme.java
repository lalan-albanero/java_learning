import java.util.Scanner;  
import java.lang.Math;  

public class basic_programme {
    static void printFibonacci(int n){
        int n1=0,n2=1,n3,i;
        for(i=0;i<n;i++){
            if(i<=1){
                System.out.println(i);
            }else{
                n3=n1+n2;
                System.out.println(n3);
                n1=n2;
                n2=n3;
            }
        }
    }
    static void isPrime(int n){
        boolean flag=true;
        if(n<2){
            System.out.print(n+" is not a Prime Number");
        }
        for(int i=2;i*i<n;i++){
            if(n%i==0){
                System.out.print(n+" is not a Prime Number");
                flag=false;
                break;
            }
        }
        if(flag==true){

            System.out.print(n+" is a Prime Number");
        }
    }
    static void isPalindrome(int n){
        int x=n;
        int rem,result=0;
        while(x>0){
            rem=x%10;
            result=result*10+rem;
            x/=10;
        }
        if(n==result){
            System.out.println(n+" is a Palindrome number.");
        }else{
            
            System.out.println(n+" is not a Palindrome number.");
        }
    }
    static void isArmstrong(int n){
        int x=n,digits=0;
        int rem,result=0;
        while(x>0){
            x/=10;
            digits++;
        }
        x=n;
        while(x>0){
            rem=x%10;
            result+=(Math.pow(rem,digits));
            x/=10;
            
        }
        if(n==result){
            System.out.println(n+" is a Armstrong number.");
        }else{
            
            System.out.println(n+" is not a Armstrong number.");
        }
    }
    static void factorial(int n){
        if(n<0){
            System.out.println("negative number");
            return;
        }
        int fact=1;
        for(int i=1;i<=n;i++){
            fact*=i;
        }
        System.out.println("Factorial of "+n+" is "+fact);
    }
    public static void main(String args[]){
        int n=343;
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter A number:-");
            n=sc.nextInt();
        }
        // printFibonacci(n);
        // isPrime(n);
        // isPalindrome(n);
        // isArmstrong(n);
        factorial(n);
        
    }
}
