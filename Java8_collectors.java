import java.util.stream.Collectors;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;

class Product {
    int id;
    String name;
    float price;

    public Product(int id, String name, float price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}

public class Java8_collectors {
    public static void main(String[] args) {
        List<Product> productsList = new ArrayList<Product>();
        // Adding Products
        productsList.add(new Product(1, "HP Laptop", 25000f));
        productsList.add(new Product(2, "Dell Laptop", 30000f));
        productsList.add(new Product(3, "Lenevo Laptop", 28000f));
        productsList.add(new Product(4, "Sony Laptop", 28000f));
        productsList.add(new Product(5, "Apple Laptop", 90000f));

        List<Float> pricelist = productsList.stream()
                .map(p -> p.price).collect(Collectors.toList());// creating as list
        System.out.println("List :" + pricelist);
        Set<Float> pricelist1 = productsList.stream()
                .map(p -> p.price).collect(Collectors.toSet()); // creating as Set
        System.out.println("Set :" + pricelist1);
        Double sumPrices = productsList.stream()
                .collect(Collectors.summingDouble(x -> x.price)); // adding all prices
        System.out.println("Sum of prices: " + sumPrices);
        Double averagePrice = productsList.stream()
                .collect(Collectors.averagingDouble(p -> p.price));
        System.out.println("Average price is: " + averagePrice);
        Long noOfElements = productsList.stream()
                .collect(Collectors.counting());
        System.out.println("Total elements : " + noOfElements);
    }
}
