class A{
    private int value;// data hiding
    void setValue(int x){ //data abstraction
        value=x;
    }
    int getValue(){
        return value;
    }

}

public class Encapsulation {
    public static void main(String[] args) {
        A ref= new A();
        // ref.value=100; can't do this.
        ref.setValue(155);
        System.out.println(ref.getValue());
    }
    
}
