// class represents user-defined exception  
class UserDefinedException1 extends Exception {
    public UserDefinedException1(String str) {
        super(str);
    }
}


public class TestThrow3 {
    public static void main(String args[]) {
        try {
            throw new UserDefinedException1("This is user-defined exception");
        } catch (UserDefinedException1 ude) {
            System.out.println("Caught the exception");
            
            System.out.println(ude.getMessage());
        }
    }
}
