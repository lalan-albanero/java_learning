// creating thraed using Runnable interface, setting priority, checking Deamon,setting daemon.

public class ThreadRunnable implements Runnable {

    public void run() {
        if(Thread.currentThread().isDaemon()){// checking current thread is daemon or not
            System.out.println("Daemon thread");
        }else{
            System.out.println("User thread");
        }
        System.out.println(" running...");
    }

    public static void main(String[] args) {
        ThreadRunnable obj = new ThreadRunnable();
        Thread t2 = new Thread(obj);
        Thread t3 = new Thread(obj);
        Thread t4 = new Thread(obj);
        System.out.println("priority of t2= "+t2.getPriority() );//to get the priority of a thread.
        System.out.println("priority of t3= "+t3.getPriority() );
        System.out.println("priority of t4= "+t4.getPriority() );
        t2.setPriority(4);// setting new priority to a thread. 
        t4.setPriority(6);
        System.out.println("priority of t2= "+t2.getPriority() );
        System.out.println("priority of t3= "+t3.getPriority() );
        System.out.println("priority of t4= "+t4.getPriority() );
        t2.start();
        t3.setDaemon(true);//setting t3 as Daemon thread.
        t3.start();
       // t3.setDaemon(true);//setting as Daemon thread after starting a thread throws exception.
    }
}
