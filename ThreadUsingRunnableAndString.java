// creating thread using runnable and string.


public class ThreadUsingRunnableAndString implements Runnable{
    public void run(){
        System.out.println("thread running...");
    }
    public static void main(String[] args) {
        Runnable r= new ThreadUsingRunnableAndString();
        Thread th1= new Thread(r,"myThread");
        th1.start();
        System.out.println(th1.getName());
    }
    
}
