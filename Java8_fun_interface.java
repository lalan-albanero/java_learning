import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

interface Doable {
    default void doIt() {
        System.out.println("Do it now");
    }
}

interface sayable extends Doable {
    void say(String msg); // abstract method
}
// @FunctionalInterface
// interface Doable extends sayable{
// // Invalid '@FunctionalInterface' annotation; Doable is not a functional
// interface
// void doIt();
// }

public class Java8_fun_interface implements sayable {
    public static void main(String[] args) {
        Java8_fun_interface j1 = new Java8_fun_interface();
        j1.say("Rahul");
        j1.doIt();
        BiConsumer<String, Integer> bicon = (name, age) -> {
            System.out.println(name + " " + age);
        };
        bicon.accept("lalan", 22);
        Function<Integer, Integer> fun1 = (a) -> (a + 10);
        System.out.println(fun1.apply(20));
        BiFunction<Integer, Integer, Integer> Bifun = (a, b) -> (a * b);
        System.out.println(Bifun.apply(10, 5));
    }

    @Override
    public void say(String name) {
        // TODO Auto-generated method stub
        System.out.println("Have anice day..." + name);

    }
}
