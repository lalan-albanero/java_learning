import java.util.Base64;
import java.util.Base64.Encoder;

public class Java8_base64 {
    public static void main(String[] args) {
        Encoder encoder=Base64.getEncoder();
        byte[] byteArr={1,2};
        byte[] byteArr2=encoder.encode(byteArr);
        System.out.println(byteArr);
        System.out.println(byteArr2);
        byte byteArr3[] = new byte[5];                // Make sure it has enough size to store copied bytes  
        int x = encoder.encode(byteArr,byteArr3);    // Returns number of bytes written  
        System.out.println("Encoded byte array written to another array: "+byteArr3);  
        System.out.println("Number of bytes written: "+x);  

        System.out.println("lalan".getBytes());
        // Encoding string  
        String str = encoder.encodeToString("JavaTpoint".getBytes());  
        System.out.println("Encoded string: "+str);  
        // Getting decoder  
        Base64.Decoder decoder = Base64.getDecoder();  
        // Decoding string  
        String dStr = new String(decoder.decode(str));  
        System.out.println("Decoded string: "+dStr);  


       // Getting encoder  
       Base64.Encoder encoder1 = Base64.getUrlEncoder();  
       // Encoding URL  
       String eStr = encoder1.encodeToString("http://www.javatpoint.com/java-tutorial/".getBytes());  
       System.out.println("Encoded URL: "+eStr);  
       // Getting decoder  
       Base64.Decoder decoder1 = Base64.getUrlDecoder();  
       // Decoding URl  
       String dStr1 = new String(decoder1.decode(eStr));  
       System.out.println("Decoded URL: "+dStr1);  

       // Getting MIME encoder  
       Base64.Encoder encoder2 = Base64.getMimeEncoder();  
       String message = "Hello, \nYou are informed regarding your inconsistency of work";  
       String eStr2 = encoder2.encodeToString(message.getBytes());  
       System.out.println("Encoded MIME message: "+eStr2);  
         
       // Getting MIME decoder  
       Base64.Decoder decoder2 = Base64.getMimeDecoder();  
       // Decoding MIME encoded message  
       String dStr2 = new String(decoder2.decode(eStr2));  
       System.out.println("Decoded message: "+dStr2);   
    }
}
