import java.util.Arrays;

public class Java8_parallelSort {
    public static void main(String[] args) {
        int[] arr = { 9, 8, 6, 7, 5, 4 };
        Arrays.parallelSort(arr,0,4);
        for (int i : arr) {
            System.out.println(i);
        }
    }
}
