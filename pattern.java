import java.util.Scanner;

public class pattern {
    static void rightTrianglePattern(int n){
        for(int i=0;i<n;i++){
            for(int j=0;j<=i;j++){
                System.out.print("* ");
            }
            System.out.print("\n");
        }
    }
    static void leftTrianglePattern(int n){
        for(int i=0;i<n;i++){

            for(int j=n-i-1;j>0;j--){
                System.out.print("  ");
            }
            for(int j=0;j<=i;j++){
                System.out.print("* ");
            }
            System.out.print("\n");
        }
    }
    static void patternPyramid(int n){
        for(int i=0;i<n;i++){

            for(int j=n-i;j>1;j--){
                System.out.print(" ");
            }
            for(int j=0;j<=i;j++){
                System.out.print("* ");
            }
            System.out.print("\n");
        }
    }
    static void patternDiamond(int n){
        for(int i=0;i<n;i++){

            for(int j=n-i;j>1;j--){
                System.out.print(" ");
            }
            for(int j=0;j<=i;j++){
                System.out.print("* ");
            }
            System.out.print("\n");
        }
        for(int i=n-2;i>=0;i--){

            for(int j=n-i;j>1;j--){
                System.out.print(" ");
            }
            for(int j=0;j<=i;j++){
                System.out.print("* ");
            }
            System.out.print("\n");
        }
    }
    static void DownwardTrianglePattern(int n){
        for(int i=n-1;i>=0;i--){
            for(int j=0;j<=i;j++){
                System.out.print("* ");
            }
            System.out.print("\n");
        }
    }
    static void MirroredRightTrianglePattern(int n){
        for(int i=n-1;i>=0;i--){
            for(int j=0;j<i;j++){
                System.out.print("  ");
            }
            for(int j=0;j<n-i;j++){
                System.out.print(" *");
            }

            System.out.print("\n");
        }
    }
    
    public static void main(String[] args) {
        int n;
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter A number:-");
            n=sc.nextInt();
        }
        MirroredRightTrianglePattern(n);
    }
}
