abstract class Animal{
    
    abstract void sound();
}

class Dog extends Animal{
    @Override
    void sound(){
        System.out.println("dog barks...");
    }
}
class Lion extends Animal{
    void sound(){
        System.out.println("lion roars...");
    }
}


class Abstraction {
    public static void main(String[] args) {
        Animal A=new Dog() ;
        Dog d= new Dog();
        Lion l= new Lion();
        d.sound();
        l.sound();
        A.sound();
    }
}
