class B {
    int x, y;

    void setValue() {
        x = 10;
        y = 20;
    }

    void setValue(int a, int b) {
        x = a;
        y = b;
    }

    void displayValue() {
        System.out.println("x= " + x + " y= " + y);
    }
}

class C extends B {
    @Override
    void setValue(int a, int b) {
        x = b;
        y = a;
    }
}

class Polymorphism {
    /**
     * @param args
     */
    public static void main(String[] args) {
        B ref = new B();
        B ref1 = new B();
        C ref2 = new C();
        ref.setValue();
        ref1.setValue(100, 200);
        ref2.setValue(400, 500);
        ref.displayValue();
        ref1.displayValue();
        ref2.displayValue();
    }
}
