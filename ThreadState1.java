// programm to test different States of threads.

class xyz implements Runnable {
    public void run() {
        try {
            Thread.sleep(100, 0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("state of t1 thread in xyz " + ThreadState1.t1.getState());
        try {
            Thread.sleep(100, 0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("in xyz 1");
        System.out.println("in xyz 2");

    }
}

public class ThreadState1 implements Runnable {
    public static ThreadState1 obj;
    public static Thread t1;

    public static void main(String[] args) {
        obj = new ThreadState1();
        t1 = new Thread(obj);
        System.out.println("Before Start() metohod on t1  " + t1.getState());
        t1.start();
        System.out.println("after Start() metohod on t1 " + t1.getState());

    }

    public void run() {
        xyz obj1 = new xyz();
        Thread t2 = new Thread(obj1);
        System.out.println("before start() metohod on t2 state of t1  " + t1.getState());
        t2.start();
        System.out.println("after start() metohod on t2 state of t1  " + t1.getState());
        System.out.println("before sleep() metohod on t2  " + t2.getState());
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("after sleep() metohod  on t2 " + t2.getState());
        try {
            // waiting for thread t2 to complete its execution
            t2.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        System.out.println("state of t1 " + t1.getState());
    }

}
