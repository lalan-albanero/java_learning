class ThreadGroupDemo implements Runnable {
    public void run() {
        System.out.println("running..." + Thread.currentThread().getName());

    }

    public static void main(String[] args) {
        ThreadGroupDemo tgd = new ThreadGroupDemo();// creating object of its own class
        ThreadGroup tg = new ThreadGroup("myThreadGroup"); // creating a parent threadGroup
        ThreadGroup tg1 = new ThreadGroup(tg, "myChildThreadGroup");// creating a childgroup
        Thread t1 = new Thread(tg, tgd, "one");//creating thread
        Thread t2 = new Thread(tg, tgd, "two");//creating thread
        Thread t3 = new Thread(tg, tgd, "three");//creating thread
        Thread[] list = new Thread[tg.activeCount()];
        ;
        t1.start();
        t2.start();
        t3.start();
        tg.list();//list all active thread ina group.
        System.out.println("groupname " + tg.getName());//getting group name
        // tg1.destroy();// to destroy a threadgroup  
        // System.out.println("groupcount " + tg.activeGroupCount());

    }
}
