// creating thraed using Thraed class

class ThreadClass extends Thread {
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {

                Thread.sleep(500);
                // Thread.sleep(-500); //thorws exception
            } catch (InterruptedException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            System.out.println("running...." + i +" "+Thread.currentThread().getName());
        }
    }

    public static void main(String[] args) {
        ThreadClass t1 = new ThreadClass();
        ThreadClass t2 = new ThreadClass();
        // t1.run();goes onto the current call stack rather than at the beginning of a
        // new call stack.
        // System.out.println("currentThread "+Thread.currentThread().getName());
        t1.start();
        // t1.setName("thread-1");
        // t1.start();// throws IllegalThreadStateException
        try {
            t1.join();
        } catch (InterruptedException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        t2.start();
        // t2.setName("thread-2");
        // try {
        //     t2.join();
        // } catch (InterruptedException e) {
        //     // TODO: handle exception
        //     e.printStackTrace();
        // }
        System.out.println("t1 State=" + t1.getState());
        System.out.println("t2 State=" + t2.getState());
        System.out.println("currentThread State "+Thread.currentThread().getState());
    }
}
