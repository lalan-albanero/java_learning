import java.util.StringJoiner;

public class Java8_stringJoiner {
    public static void main(String[] args) {
        StringJoiner sj = new StringJoiner(",", "{", "}");
        sj.setEmptyValue("it's empty...");
        System.out.println(sj);
        sj.add("rosan");
        sj.add("rahul");
        StringJoiner sj1 = new StringJoiner(":", "[", "]");
        sj1.add("sagar");
        sj1.add("vishal");
        sj1.add("lalan");
        System.out.println(sj);
        System.out.println(sj1);
        System.out.println(sj1.merge(sj));
        StringJoiner sj2 = sj1.merge(sj);
        System.out.println(sj2.toString().charAt(20));
        System.out.println(sj2.length());
    }
}
