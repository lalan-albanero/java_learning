interface default_methods {
    // default method
    default int add(int a, int b) {
        return (a + b);
    }

    // abstract method
    void say();

    // static method
    static void sayLouder(String msg) {
        System.out.println(msg);
    }
}

public class Java8_Defalut implements default_methods {
    public void say() {
        System.out.println("I have nothing to say....");
    }

    public static void main(String[] args) {
        Java8_Defalut j1 = new Java8_Defalut();
        j1.say();
        System.out.println(j1.add(2, 3));
        default_methods.sayLouder("louder is not enough...");
    }
}

/*
 * After having default and static methods inside the interface, we think about
 * the need of abstract class in Java. An interface and an abstract class is
 * almost similar except that you can create constructor in the abstract class
 * whereas you can't do this in interface.
 */
