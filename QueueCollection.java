import java.util.PriorityQueue;

public class QueueCollection {
    public static void main(String[] args) {
        PriorityQueue<String> que = new PriorityQueue<String>();
        que.add("one");
        que.add("two");
        que.add("three");
        que.add("five");
        que.add("rahul");
        que.add("five");
        System.out.println(que.peek());
        System.out.println(que);
        java.util.Iterator<String> itr = que.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}
