// implementation thraed pool.
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Worker implements Runnable{
    private String message;  
    public Worker(String s){  
        this.message=s;  
    }  
    public void run(){
        System.out.println(Thread.currentThread().getName()+" (Start) message = "+message);  
        try {  Thread.sleep(2000);  } catch (InterruptedException e) { e.printStackTrace(); }  
        System.out.println(Thread.currentThread().getName()+" (End)");//prints thread name  
    }
}


public class ThreadPool {
    public static void main(String[] args) {
        ExecutorService executor= Executors.newFixedThreadPool(5) ; //cerating pool of size 5
        ExecutorService executor1= Executors.newFixedThreadPool(5) ;
        for(int i=1;i<8;i++){
            Runnable worker= new Worker(""+i);
            executor.execute(worker);
            executor1.execute(worker);

        }
        executor.shutdown();  
        while (!executor.isTerminated()) {   }  
  
        System.out.println("Finished all threads");  
    }  
    
}
