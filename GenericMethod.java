public class GenericMethod {

    public static <T> void printArray(T[] inputArray) {
        for (T ele : inputArray) {
            System.out.printf("%s, ", ele);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Integer[] arr = { 1, 2, 3, 4, 5 };
        Double[] doubleArray = { 1.1, 2.2, 3.3, 4.4 };
        Character[] charArray = { 'H', 'E', 'L', 'L', 'O' };
        printArray(arr);
        printArray(doubleArray);
        printArray(charArray);
    }

}
