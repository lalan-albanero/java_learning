import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

public class javaCollection {
    public static void main(String[] args) {
        ArrayList <String> arr= new ArrayList<String>();
        arr.add("A");
        arr.add("C");
        arr.add("B");
        // System.out.println(arr);
        arr.add(1,"abc");
        // arr.addAll(arr);
        // System.out.println(arr);
        // Iterator itr=arr.iterator();
        // while(itr.hasNext()){
        //     System.out.println(itr.next());
        // }
        LinkedList <String>link=new LinkedList<>();
        link.add("x");
        link.add("z");
        link.add("y");
        link.addAll(arr);
        // System.out.println(link.size());
        // System.out.println(arr);
        Stack <String> st=new Stack<>();
        st.push("a1");
        st.push("a2");
        st.push("a3");
        
        System.out.println(st);
        st.pop();
        System.out.println(st.contains("a1"));
        st.addAll(arr);
        Collections.sort(st);
        System.out.println(st);
        Collections.sort(arr);
        System.out.println(arr);

    }
}
