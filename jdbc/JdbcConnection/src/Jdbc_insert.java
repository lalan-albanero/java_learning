import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Jdbc_insert {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "password");
            PreparedStatement psmt = con.prepareStatement("insert into emp(empName,empSalary) values(?,?)");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            while (true) {
                System.out.println("Enter empName: ");
                String name = br.readLine();
                System.out.println("Enter empSalary: ");
                int salary = Integer.parseInt(br.readLine());

                psmt.setString(1, name);
                psmt.setInt(2, salary);

                int count = psmt.executeUpdate();

                if (count > 0) {
                    System.out.println(count + " row inserted...");
                } else {
                    System.out.println(" row not inserted...");
                }
                System.out.println("Do you want to insert more row ...type {yes/no}");
                String choice=br.readLine();
                if(choice.equalsIgnoreCase("no")){
                    con.close();
                    break;
                }else if(choice.equalsIgnoreCase("yes")){
                    continue;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
