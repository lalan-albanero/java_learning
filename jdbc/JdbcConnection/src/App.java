import java.lang.Class;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "password");
            Statement smt= con.createStatement(0, 0);
            smt.executeUpdate("create table emp(empId int auto_increment,empName varchar(200),empSalary int, primary key(empId))" );
            con.close();
            System.out.println("Hello, World! good to go");
        }catch(Exception e){
            System.out.println(e);
        }
    }
}
