import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.Statement;

public class Jdbc_delete {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "password");
            Statement smt = con.createStatement();

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            while (true) {

                System.out.println("Enter empId of employee you want to delete: ");
                int id = Integer.parseInt(br.readLine());
                String str1 = "delete from emp  where empId=" + id;
                // System.out.println(str1);
                int count = smt.executeUpdate(str1);
                if (count > 0) {
                    System.out.println(count + " row deleted...");
                } else {
                    System.out.println(" row not deleted...");
                }
                System.out.println("Do you want to delete more row ...type {yes/no}");
                String choice = br.readLine();
                if (choice.equalsIgnoreCase("no")) {
                    con.close();
                    break;
                } else if (choice.equalsIgnoreCase("yes")) {
                    continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
