import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Jdbc_update {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "password");
            Statement smt = con.createStatement(0, 0);
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Enter empId of employee you want to upadte: ");
            int id = Integer.parseInt(br.readLine());
            System.out.println("Enter new empName: ");
            String name = br.readLine();
            System.out.println("Enter new empSalary: ");
            int salary = Integer.parseInt(br.readLine());
            String str1 = "update emp set empName=" +'"'+ name+'"' + ", empSalary=" + salary+" where empId="+id;
            // System.out.println(str1);
            int count = smt.executeUpdate(str1);
            if (count > 0) {
                System.out.println(count + " row updated...");
            } else {
                System.out.println(" row not updated...");
            }
            con.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
