import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Jdbc_Select {
    public static void main(String[] args) {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "password");
            Statement smt= con.createStatement(0, 0);
            ResultSet rs= smt.executeQuery("select * from emp");
            System.out.println("empId   empName         empSalary");
            
            while(rs.next()){
                int empId=rs.getInt(1);
                String empName=rs.getString(2);
                int empSalary=rs.getInt(3);
                System.out.println(empId+"    "+empName+"        "+empSalary);
            }
            con.close();
        }catch(Exception e){
            System.out.println(e);
        }
    }
}
