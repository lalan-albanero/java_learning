class Table {
    // synchronized void printTable(int n){
    synchronized static void printTable(int n) {
        // synchronized (this) {

            for (int i = 1; i <= 10; i++) {
                System.out.println(n * i);
                try {
                    Thread.sleep(400);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        // }
    }
}

public class TestSynchronization {
    public static void main(String[] args) {
        Table tab = new Table();
        Thread t1 = new Thread() {
            public void run() {
                // tab.printTable(1);
                Table.printTable(1);
            }
        };
        Thread t2 = new Thread() {
            public void run() {
                // tab.printTable(10);
                Table.printTable(10);
            }
        };
        t1.start();
        t2.start();
    }

}
