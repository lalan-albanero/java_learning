public class Counter {
    // static int count=0;
    int count=0;
    Counter(){
        count++;
        System.out.println(this.count);
    }
    Counter(int x){
        this();
        count=x;
        System.out.println(count);
    }
    // static void disp(){
    //     System.out.println(count);
    // }
    public static void main(String[] args) {
        Counter a=new Counter(5);
        // Counter b=new Counter();
        // Counter c=new Counter();
    }
}
