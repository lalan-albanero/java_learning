import java.util.ArrayList;
import java.util.List;

public class Java8_forEach {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("E");
        list.add("D");
        list.add("C");
        list.add("B");
        list.add("A");
        list.forEach(str -> System.out.print(str+" "));
        list.forEach(System.out::println);
        list.stream().forEachOrdered(System.out::println);

    }
}
